const cells = [];

class Cell {
    constructor(cellNo) {
        this.cellNo = cellNo;
        this.played = false;
        this.status;
    }

    setX() {
        this.played = true
        this.status = "x"
        return "<span class='blue'>X</span>"
        // return "<i class=\"fas fa-times blue\"></i>"
    }

    setO() {
        this.played = true;
        this.status = "o";
        return "<span class='red'>O</span>"
        // return "<i class=\"far fa-circle red\"></i>"
    }
}

const gameContainer = document.querySelector(".game-container");
const startGameBtn = document.querySelector(".overlay-content-start");
const overlay = document.querySelector(".overlay");
const startNewGame = document.querySelector(".new-game");
const restart = document.querySelector(".restart");
const turn = document.querySelector(".turn");
let player1Name = "Player 1";
let player2Name = "Player 2";
let currentPlayer;
let gameActive = false;
const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];

function writePlayerNames (classname, playerName) {
    const playerTag = document.querySelector(`.${classname}`);
    playerTag.innerText = playerName ? playerName : "Player 1";
    playerTag.style.top = `${window.innerHeight*0.45 - playerTag.offsetHeight/2}px`;
}

function startGame() {
    currentPlayer = 1;
    whosTurn()
    cells.length = 0;
    gameContainer.innerHTML = "";
    writePlayerNames("player1", player1Name);
    writePlayerNames("player2", player2Name);
    for (let i = 0; i < 9; i++) {
        const gameCell = document.createElement("div");
        gameCell.classList.add("game-container-cell");
        gameCell.dataset.cellNo = `${i}`;
        gameContainer.append(gameCell);
        cells.push(new Cell(i));
    }
    gameActive = true;
}

startGame();

startGameBtn.addEventListener("click", () => {
    player1Name = document.querySelector("#player1").value ?
        document.querySelector("#player1").value :
        'Player 1';
    player2Name = document.querySelector("#player2").value ?
        document.querySelector("#player2").value :
        'Player 2';
    document.querySelector("#player1").value = "";
    document.querySelector("#player2").value = "";

    startGame();
    overlay.classList.add("d-none");
})

gameContainer.addEventListener("click", (event) => {
    if (event.target.dataset.cellNo && gameActive) {
        const clickedCell = cells[event.target.dataset.cellNo];
        if (!clickedCell.played) {
            event.target.innerHTML = currentPlayer ? clickedCell.setX() : clickedCell.setO();
            currentPlayer = !currentPlayer;
            checkIsThereAWinner();
        }
    }

})


function checkIsThereAWinner() {
    let roundWon = false;
    for (let i = 0; i <= 7; i++) {
        const winCondition = winningConditions[i];
        if (cells[winCondition[0]].status && cells[winCondition[1]].status && cells[winCondition[2]].status){
            if (cells[winCondition[0]].status === cells[winCondition[1]].status && cells[winCondition[0]].status === cells[winCondition[2]].status) {
                roundWon = true;
                break
            }
        }
    }
    if (roundWon) {
        gameActive = false;
        if(currentPlayer) {
            gameContainer.innerHTML+= `<p class=\"message red\">${player2Name} won the game</p>`;
        }
        else {
            gameContainer.innerHTML+= `<p class=\"message blue\">${player1Name} won the game</p>`;
        }
    }
    else if (!cells.some( (cell) => !cell.played)){
        gameContainer.innerHTML+= "<p class=\"message\">It's a draw</p>"
        gameActive = false;
    }
    else {
        whosTurn()
    }
}

startNewGame.addEventListener("click", () => {
    overlay.classList.remove("d-none");
})

function whosTurn () {
    if (currentPlayer){
        turn.innerText = `${player1Name}'s turn`;
    }
    else {
        turn.innerText = `${player2Name}'s turn`;
    }
}

restart.addEventListener("click", () => {
    startGame();
    gameActive = true;
})